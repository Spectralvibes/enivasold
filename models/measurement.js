var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var measurementSchema = new Schema({
    userid: {type: String, required:'Please enter userid'},
    age: {type: Number, required:'Please enter age'},
    bodyAge: {type: Number, required:'Please enter bodyAge'},
    height: {type: String, required:'Please enter height'},
    weight: {type: String, required:'Please enter weight'},
    bmr: {type: String, required:'Please enter BMR'},
    bmi: {type: String, required: 'Please enter BMI'},
    bodyFat: {type: String, required: 'Please enter Body Fat %'},
    visceralFat: {type: String, required: 'Please enter Visceral Flat %'},
    subCuteneousFat:{
            wb: {type: String, required: 'Please enter WB fat %'},
            trunk: {type: String, required: 'Please enter trunk fat %'},    
            arms: {type: String, required: 'Please enter arms fat %'},    
            legs: {type: String, required: 'Please enter legs fat %'}
        },
    skeletalMuscle:{
            wb: {type: String, required: 'Please enter WB muscle %'},
            trunk: {type: String, required: 'Please enter trunk muscle %'},    
            arms: {type: String, required: 'Please enter arms muscle %'},    
            legs: {type: String, required: 'Please enter legs muscle %'}
        },
    created: {type: Date, default: Date.now}
});

var Measurement = mongoose.model('measurement',measurementSchema);

module.exports= {Measurement:Measurement};