var mongoose = require('mongoose');
var userService = require('../services/user-service');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstname: {type: String, required:'Please enter your firstname'},
    lastname: {type: String, required:'Please enter your lastname'},
    email: {type: String, required:'Please enter your email'},
    password: {type: String, required:'Please enter your password'},
    mobile: {type: String, required:'Please enter your mobile number'},
    status: {type: String, default: 'Active'},
    created: {type: Date, default: Date.now}
});


userSchema.path('email').validate(function(value,next){
   userService.findUser(value,function(err,user){
      if(err){
          return next(false);
      } 
      next(!user);
   }); 
},'Email already exists!');


var User = mongoose.model('Lifestyler',userSchema);

module.exports= {User:User};