var Measurement = require('../models/measurement').Measurement;

exports.addMeasurement = function(measurement,next){
    var newMeasurement= new Measurement({
            userid: measurement.userid,
            weight: measurement.weight,
            bmr: measurement.bmr,
            bmi: measurement.bmi,
            created: measurement.created
    });
    
    newMeasurement.save(function(err){
        if(err){
            return next(err);
        }
        next(null);
    });
};

exports.findMeasurement= function(userid,next){
    Measurement.find({userid: userid},{userid: 0,_id: 0}, function(err, measurement){
        next(err,measurement);        
    });
};
