var express = require('express');
var router = express.Router();
var measurementService = require('../../services/measurement-service');
var _=require('underscore');


/* GET measurements. */
router.get('/measurements', function(req, res, next) {
  
  measurementService.findMeasurement(req.user._id, function(err,measurement){
      if(err){
          res.send(err);
      }else{
          /* //Underscore to emit userid
          res.json(_.map(measurement, function(currentObject) { 
            return _.pick(currentObject,'weight','bmr','bmi')
          }));
          */
          res.json(measurement);
      }
  });
  
});



module.exports = router;