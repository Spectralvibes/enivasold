//Update: Broken api; Need changes
var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../../config');


/* POST signin page. */
router.post('/signup',function(req, res, next) {
    req.session.orderId = 12345;
    if (req.body.rememberMe) {
      req.session.cookie.maxAge = config.cookieMaxAge;
    }
    next();
  },passport.authenticate('local',{
    failureRedirect: '/',
    successRedirect: '/dashboard',
    failureFlash: 'Email or password is incorrect!'
}));

module.exports = router;