var express = require('express');
var router = express.Router();
var restrict = require('../auth/restrict');

/* GET Dashboard page. */
router.get('/',restrict, function(req, res, next) {
    var vm = {
        title: 'Lifestyle: Dashboard',
        orderId: req.session.orderId,
        firstname: req.user ? req.user.firstname : null,
        lastname: req.user ? req.user.lastname : null
    }
    res.render('dashboard/index', vm);
});

module.exports = router;