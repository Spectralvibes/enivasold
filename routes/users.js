var express = require('express');
var router = express.Router();

var userService = require('../services/user-service');


/* Signin */
router.get('/signin', function(req, res, next) {
    const vm = {
      title: "Lifestyle"  
    };
  res.render('users/signin',vm);
});


/* Signup. */
router.get('/signup', function(req, res, next) {
    const vm = {
      title: "Lifestyle"  
    };
  res.render('users/signup',vm);
});


/* Signup. */
router.post('/signup', function(req, res, next) {
    userService.addUser(req.body, function(err){
        if(err)
        {
          const vm = {
            title: "Lifestyle: Sign up",
            input: req.body,
            error: err
          };
          delete vm.input.password;
          return res.render('users/signup',vm);
        };
        req.login(req.body, function(err){
          res.redirect('/dashboard');
        });
    });    
});

router.get('/signout',function(req,res,next){
  req.logout();
  req.session.destroy();
  res.redirect('/');
});


module.exports = router;
