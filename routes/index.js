var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config');


/* GET index page. */
router.get('/', function(req, res, next) {
  if(req.user){
    return res.redirect('/dashboard');
  }
  var vm = {
    title: 'Lifestyle',
    error: req.flash('error')
  };
  res.render('index', vm);
});

/* POST signin page. */
router.post('/',function(req, res, next) {
    req.session.orderId = 12345;
    if (req.body.rememberMe) {
      req.session.cookie.maxAge = config.cookieMaxAge;
    }
    next();
  },passport.authenticate('local',{
    failureRedirect: '/',
    successRedirect: '/dashboard',
    failureFlash: 'Email or password is incorrect!'
}));

module.exports = router;
