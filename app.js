var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var expressSession = require('express-session');
var connectMongo = require('connect-mongo');

var flash = require('connect-flash');

var config = require('./config');
var index = require('./routes/index');
var users = require('./routes/users');
var dashboard = require('./routes/dashboard');
// API Routes
var measurements = require('./routes/api/measurements');
var signin = require('./routes/api/signin');
var signup = require('./routes/api/signup');


var MongoStore = connectMongo(expressSession);

var passportConfig = require('./auth/passport-config');
var restrict = require('./auth/restrict');

passportConfig();


var options = { promiseLibrary: require('bluebird') };
mongoose.connect(config.mongoUri,options);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressSession(
  {
    secret : 'Hurry up!',
    saveUninilialized : false,
    resave: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  }
));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());



app.use('/', index);
app.use('/users', users);
app.use(restrict);
app.use('/dashboard', dashboard);

//REST api routes
app.use('/api', measurements);
app.use('/api', signin);
app.use('/api', signup);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
