(function() {
        'use strict';

        angular
            .module('dashboard')
            .config(config);

        config.$inject = ['$routeProvider'];

        function config($routeProvider) {
            $routeProvider
                .when('/dashboard', {
                    templateUrl: '/javascripts/app/lifestyle/dashboard.html',
                    controller: 'lifestyleController',
                    controllerAs: 'vm'
                });
        }
}());