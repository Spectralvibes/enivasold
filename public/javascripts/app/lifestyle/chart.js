$(function () {
        
        $('#chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Weight loss record'
            },
            xAxis: {
                categories: [   "2016-01-01",   "2016-02-01",   "2016-03-01",   "2016-04-01",   "2016-05-01",   "2016-06-01",   "2016-07-01",   "2016-08-01",   "2016-09-01",   "2016-10-01",   "2016-11-01",   "2016-12-01",   "2017-01-01" ],
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' Kg'
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Weight',
                data: [88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77]
            },
            {
                name: 'FAT',
                data: [30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19]
            },
            {
                name: 'Sat',
                data: [30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19]
            }]
        });
    });