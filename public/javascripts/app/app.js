'use strict';

angular
    .module('dashboard',['ngRoute'])
    .config(['$routeProvider',function($routeProvider){
        $routeProvider
        .otherwise({redirectTo:'/dashboard'})
    }]);